var express = require('express');
var router = express.Router();
const firebase = require('firebase')
const admin = require('firebase-admin')
const serviceAccount = require('../google-service-account.json')
const json_data = require('./db_calibrate.json')

firebase.initializeApp({
  apiKey: process.env.FDB_API_KEY,
  authDomain: process.env.FDB_DOMAIN,
  databaseURL: process.env.FDB_HOST,
  storageBucket: 'bucket.appspot.com',
})

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://quack-services-quackapipolda-staging.firebaseio.com/',
})

router.get('/', function(req, res, next) {
  var db = admin.database();
  var ref = db.ref("/clean_water");
  ref.once("value", function(snapshot) {
    let fdb_data = []
    let results = snapshot.val()
    for (result of results) {
      if (result) {
        fdb_data.push(result)
      }
    }
    res.send({status: "success", data: fdb_data});
  });
});

router.post('/', function(req, res, next) {
  let req_data = req.body
  let calibrations = json_data.data

  for (calibrate of calibrations) {
    if (calibrate.id === req_data.id) {
      req_data.standard_quality = {
        "max_temperature": calibrate.max_temperature,
        "max_tds_quality": calibrate.max_tds_quality,
        "max_water_clarity": calibrate.max_water_clarity,
        "min_temperature": calibrate.min_temperature,
        "min_tds_quality": calibrate.min_tds_quality,
        "min_water_clarity": calibrate.min_water_clarity
      }
    }
  }

  firebase.database().ref('clean_water/' + req_data.id + '/').set(req_data)
  res.send({status: "success", data: req_data})
});

module.exports = router;
